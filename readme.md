# LSM6DS3TR-C Rust

[![Crates.io](https://img.shields.io/crates/v/lsm6ds3tr)](https://crates.io/crates/lsm6ds3tr)
[![Docs](https://docs.rs/lsm6ds3tr/badge.svg)](https://docs.rs/lsm6ds3tr/latest/lsm6ds3tr/)

LSM6DS3TR-C 6-axis (DOF) IMU accelerometer & gyroscope rust driver library.

_Inspired by [LSM9DS1 rust driver](https://github.com/lonesometraveler/lsm9ds1)._
